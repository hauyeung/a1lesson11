package com.example.lesson11;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import java.util.List;
import java.util.concurrent.ExecutionException;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.ListActivity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;
import android.widget.ListView;

public class MainActivity extends ListActivity {
	  
	  private ImageAdapter adapter;
	  ListView listview;
	  
	  @Override
	  public void onCreate(Bundle savedInstanceState) {
		   super.onCreate(savedInstanceState);
		   setContentView(R.layout.main);

		   this.adapter = new ImageAdapter(this);
		   setListAdapter(adapter);

	  }
	  
	  public void onLoadClicked(View view) {
	   // No need to trigger more than once
	   view.setVisibility(View.GONE);
	   
	   // Download the images and add them to the adapter
	   //List<String> urls = downloadImageUrls();
	   List<String> urls = null;
	try {
		urls = (List<String>) new downloadImageUrls().execute().get();
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (ExecutionException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	   if (urls!=null) {
	     for (String url : urls) {
	      //Bitmap image = downloadImage(url);
	    	Bitmap image = null;
			try {
				image = (Bitmap) new downloadImage(url).execute().get();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	
			if (image != null)
				adapter.addItem(image);
	     }
	   }
	  }
	  
	  public List<String> downloadImageUrls() {
	   HttpGet get = new HttpGet("http://www.reddit.com/r/aww.json");
	   AndroidHttpClient client = AndroidHttpClient.newInstance("android");
	   //DefaultHttpClient client = new DefaultHttpClient();
	   try {
	     final List<String> result = client.execute(get, new MyJsonResponseHandler());
	     return result;
	   } catch (ClientProtocolException e) {
	     e.printStackTrace();
	   } catch (IOException e) {
	     e.printStackTrace();
	   } finally {
	     client.close();
	   }
	   return null;
	  }
	  
	  public Bitmap downloadImage(String url) {
	   try {
	     URL u = new URL(url);
	     InputStream is = u.openStream();
	     Bitmap b = BitmapFactory.decodeStream(is);
	     return b;
	   } catch (MalformedURLException e) {
	     e.printStackTrace();
	   } catch (IOException e) {
	     e.printStackTrace();
	   }
	   return null;
	  }
	  
	  private class downloadImageUrls extends AsyncTask<Void,Void,List<String>>
	  {

		@Override
		protected List<String> doInBackground(Void... params) {
			// TODO Auto-generated method stub
			return downloadImageUrls();
		}
		  
	  }
	  
	 private class downloadImage extends AsyncTask<Void,Void,Bitmap>
	 {
		 String url;
		 public downloadImage(String url)
		 {
			 this.url = url;
		 }
		@Override
		protected Bitmap doInBackground(Void... params) {
			// TODO Auto-generated method stub
			return downloadImage(url);
		}
		 
	 }
	  
	}
