package com.example.lesson11;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class MyJsonResponseHandler implements ResponseHandler<List<String>> {

	   @Override
	   public List<String> handleResponse(HttpResponse response) throws ClientProtocolException, IOException {
	     // Loop through the results and grab the thumbnail URLs 
	     String data = EntityUtils.toString(response.getEntity());
	     ArrayList<String> results = new ArrayList<String>();
	     try {
	      JSONObject root = new JSONObject(data);
	      JSONArray children = root.getJSONObject("data").getJSONArray("children");
	      
	      for (int i = 0; i < children.length(); i++) {
	        JSONObject result = children.getJSONObject(i);
	        String thumb = result.getJSONObject("data").getString("thumbnail");
	        results.add(thumb);
	        
	        // limit the results size to 6 in order to be nice to their servers
	        if (results.size() >= 6) break;
	      }
	      
	     } catch (JSONException e) {
	      e.printStackTrace();
	     }
	     
	     return results;
	   }
	   
	  }