package com.example.lesson11;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

class ImageAdapter extends BaseAdapter {
	   
	   private List<Bitmap> images;
	   private final Context context;

	   public ImageAdapter(Context context) {
	     super();
	     this.context = context;
	     images = new ArrayList<Bitmap>();	    
	   }
	   
	   public void updateImages(List<Bitmap> images) {
	     this.images = images;
	     notifyDataSetChanged();
	   }
	   
	   @Override
	   public int getCount() {
	     return images != null ? images.size() : 0;
	   }
	   
	   @Override
	   public Bitmap getItem(int position) {
	     return images != null ? images.get(position) : null;
	   }

	   @Override
	   public long getItemId(int position) {
	     return position;
	   }
	   
	   @Override
	   public View getView(int position, View convertView, ViewGroup parent) {
	     final View view;
	     if (convertView == null || !(convertView instanceof ImageView)) {
	      view = LayoutInflater.from(context).inflate(R.layout.image_view, null);
	     } else {
	      view = convertView;
	     }
	     final ImageView imageView = (ImageView) view.findViewById(R.id.image);
	     imageView.setImageBitmap(getItem(position));
	     
	     return view;
	   }

	   public void addItem(Bitmap bitmap) {
	     images.add(bitmap);
	     notifyDataSetChanged();
	   }
	  }
	  